import React from 'react';

const Button = props => {
	let type = "btn btn-";
	type = type + props.type;
	return (
		<button
			className={
				type
			}
			onClick={props.onClick}
		>
			{props.title}
		</button>
	);
};

export default Button;